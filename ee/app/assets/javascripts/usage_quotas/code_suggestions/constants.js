import { PROMO_URL } from 'jh_else_ce/lib/utils/url_utility';
import { helpPagePath } from '~/helpers/help_page_helper';

export const ADD_ON_CODE_SUGGESTIONS = 'CODE_SUGGESTIONS';
export const FIELD_KEY_CODE_SUGGESTIONS_ADDON = 'codeSuggestionsAddon';
export const codeSuggestionsDescriptionLink = helpPagePath(
  'user/project/repository/code_suggestions.html',
);
export const codeSuggestionsLearnMoreLink = `${PROMO_URL}/solutions/code-suggestions/`;

export const salesLink = `${PROMO_URL}/sales/`;
